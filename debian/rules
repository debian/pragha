#!/usr/bin/make -f

# Enable all hardening options
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

%:
	dh $@

# Use xfce autogen
override_dh_autoreconf:
	NOCONFIGURE=1 dh_autoreconf xdt-autogen

# Do not generate a trigger for ldconfig, since all libraries shipped by pragha
# are plugins and are not installed in ld's default search path.
override_dh_makeshlibs:
	dh_makeshlibs --no-scripts

# Remove .la files
override_dh_install:
	find debian -name "*.la" -exec rm {} \;
	dh_install

# Use the --keep option with dh_installchangelogs, so that the upstream
# changelog file, i.e.: ChangeLog, gets installed with the name expected by
# Debian, i.e.: changelog.
override_dh_installchangelogs:
	dh_installchangelogs --keep

# Clean build files not removed by upstream clean rules
override_dh_clean:
	dh_clean
	rm -f config.h config.log config.status libtool stamp-h1
	rm -f po/*gmo po/Makefile.in po/POTFILES

override_dh_missing:
	dh_missing --fail-missing
