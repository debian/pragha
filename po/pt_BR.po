# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Rafael Fontenelle <rffontenelle@gmail.com>, 2012
msgid ""
msgstr ""
"Project-Id-Version: Pragha\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-26 10:11-0300\n"
"PO-Revision-Date: 2014-12-21 17:03+0000\n"
"Last-Translator: matias <mati86dl@gmail.com>\n"
"Language-Team: Portuguese (Brazil) (http://www.transifex.com/p/Pragha/"
"language/pt_BR/)\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. GtkInfoBar has undocumented behavior for GTK_RESPONSE_CANCEL
#: ../src/info-bar-import-music.c:80 ../src/info-bar-import-music.c:127
#: ../src/info-bar-import-music.c:170
msgid "_No"
msgstr ""

#: ../src/info-bar-import-music.c:81 ../src/info-bar-import-music.c:128
#: ../src/info-bar-import-music.c:171
msgid "_Yes"
msgstr ""

#: ../src/info-bar-import-music.c:83
#, c-format
msgid "Would you like to import %s to library?"
msgstr "Você deseja importar %s para a biblioteca?"

#: ../src/info-bar-import-music.c:130
msgid "Would you like to update your music library?"
msgstr ""

#: ../src/info-bar-import-music.c:173
msgid "Some changes need restart pragha."
msgstr ""

#: ../src/pragha-cmdline.c:192
msgid "FILENAME"
msgstr ""

#: ../src/pragha-cmdline.c:251
msgid "Use --help to see a full list of available command line options."
msgstr ""

#: ../src/pragha-equalizer-dialog.c:50
msgid "Disabled"
msgstr "Desabilitado"

#: ../src/pragha-equalizer-dialog.c:51
msgid "Classical"
msgstr "Clássico"

#: ../src/pragha-equalizer-dialog.c:52
msgid "Club"
msgstr "Club"

#: ../src/pragha-equalizer-dialog.c:53
msgid "Dance"
msgstr "Dance"

#: ../src/pragha-equalizer-dialog.c:54
msgid "Full Bass"
msgstr "Grave"

#: ../src/pragha-equalizer-dialog.c:55
msgid "Full Bass and Treble"
msgstr "Grave e agudo"

#: ../src/pragha-equalizer-dialog.c:56
msgid "Full Treble"
msgstr "Agudo"

#: ../src/pragha-equalizer-dialog.c:57
msgid "Laptop Speakers and Headphones"
msgstr "Fones e microfones de laptop"

#: ../src/pragha-equalizer-dialog.c:58
msgid "Large Hall"
msgstr ""

#: ../src/pragha-equalizer-dialog.c:59
msgid "Live"
msgstr "Ao vivo"

#: ../src/pragha-equalizer-dialog.c:60
msgid "Party"
msgstr "Festa"

#: ../src/pragha-equalizer-dialog.c:61
msgid "Pop"
msgstr "Pop"

#: ../src/pragha-equalizer-dialog.c:62
msgid "Reggae"
msgstr "Reggae"

#: ../src/pragha-equalizer-dialog.c:63
msgid "Rock"
msgstr "Rock"

#: ../src/pragha-equalizer-dialog.c:64
msgid "Ska"
msgstr "Ska"

#: ../src/pragha-equalizer-dialog.c:65
msgid "Smiley Face Curve"
msgstr ""

#: ../src/pragha-equalizer-dialog.c:66
msgid "Soft"
msgstr "Soft"

#: ../src/pragha-equalizer-dialog.c:67
msgid "Soft Rock"
msgstr "Soft Rock"

#: ../src/pragha-equalizer-dialog.c:68
msgid "Techno"
msgstr "Techno"

#: ../src/pragha-equalizer-dialog.c:69
msgid "Custom"
msgstr ""

#: ../src/pragha-equalizer-dialog.c:419
msgid "Equalizer"
msgstr ""

#: ../src/pragha-filter-dialog.c:218 ../src/pragha-library-pane.c:506
#: ../src/pragha-tags-dialog.c:114 ../plugins/notify/pragha-notify-plugin.c:166
msgid "Unknown Artist"
msgstr "Artista Desconhecido"

#: ../src/pragha-filter-dialog.c:219 ../src/pragha-library-pane.c:513
#: ../src/pragha-library-pane.c:518 ../src/pragha-tags-dialog.c:115
#: ../plugins/notify/pragha-notify-plugin.c:167
msgid "Unknown Album"
msgstr "Álbum Desconhecido"

#. The search dialog
#: ../src/pragha-filter-dialog.c:369
msgid "Search in playlist"
msgstr ""

#: ../src/pragha-filter-dialog.c:372
msgid "_Close"
msgstr ""

#: ../src/pragha-filter-dialog.c:375 ../src/pragha-playlist.c:209
msgid "Add to playback queue"
msgstr "Adicionar à fila de reprodução"

#: ../src/pragha-filter-dialog.c:376
msgid "_Jump to"
msgstr ""

#: ../src/pragha-library-pane.c:132
msgid "_Skip"
msgstr "_Pular"

#: ../src/pragha-library-pane.c:133
msgid "S_kip All"
msgstr "Pu_lar Todas"

#: ../src/pragha-library-pane.c:134
msgid "Delete _All"
msgstr "Deletar _Todas"

#: ../src/pragha-library-pane.c:188
msgid "_Expand library"
msgstr "E_xpandir biblioteca"

#: ../src/pragha-library-pane.c:190
msgid "_Collapse library"
msgstr "E_sconder biblioteca"

#: ../src/pragha-library-pane.c:192 ../src/pragha-library-pane.c:1585
msgid "Folders structure"
msgstr "Estrutura de pastas"

#: ../src/pragha-library-pane.c:194 ../src/pragha-library-pane.c:1594
#: ../src/pragha-playlist.c:115 ../src/pragha-playlist.c:3371
#: ../src/pragha-playlist.c:3547 ../src/pragha-statusicon.c:180
#: ../src/pragha-tags-dialog.c:168 ../src/pragha-tags-dialog.c:1082
msgid "Artist"
msgstr "Artista"

#: ../src/pragha-library-pane.c:196 ../src/pragha-library-pane.c:1603
#: ../src/pragha-playlist.c:116 ../src/pragha-playlist.c:3372
#: ../src/pragha-playlist.c:3548 ../src/pragha-statusicon.c:181
#: ../src/pragha-tags-dialog.c:169 ../src/pragha-tags-dialog.c:1087
msgid "Album"
msgstr "Álbum"

#: ../src/pragha-library-pane.c:198 ../src/pragha-library-pane.c:1612
#: ../src/pragha-playlist.c:117 ../src/pragha-playlist.c:3373
#: ../src/pragha-playlist.c:3549 ../src/pragha-tags-dialog.c:170
#: ../src/pragha-tags-dialog.c:1092
msgid "Genre"
msgstr "Gênero"

#: ../src/pragha-library-pane.c:200 ../src/pragha-library-pane.c:1624
msgid "Artist / Album"
msgstr "Artista / Álbum"

#: ../src/pragha-library-pane.c:202 ../src/pragha-library-pane.c:1648
msgid "Genre / Album"
msgstr "Gênero / Álbum"

#: ../src/pragha-library-pane.c:204 ../src/pragha-library-pane.c:1636
msgid "Genre / Artist"
msgstr "Gênero / Artista"

#: ../src/pragha-library-pane.c:206 ../src/pragha-library-pane.c:1663
msgid "Genre / Artist / Album"
msgstr "Gênero / Artista / Álbum"

#. Playlist and Radio tree
#: ../src/pragha-library-pane.c:242
msgid "_Add to current playlist"
msgstr "_Adicionar para a lista de reprodução atual"

#: ../src/pragha-library-pane.c:244
msgid "_Replace current playlist"
msgstr "_Substituir lista de reprodução atual"

#: ../src/pragha-library-pane.c:246
msgid "Replace and _play"
msgstr "Substituir e _reproduzir"

#: ../src/pragha-library-pane.c:248 ../src/pragha-playlists-mgmt.c:514
msgid "Rename"
msgstr "Renomear"

#: ../src/pragha-library-pane.c:250
msgid "Delete"
msgstr "Apagar"

#: ../src/pragha-library-pane.c:252 ../src/pragha-menubar.c:222
#: ../src/pragha-menubar.c:227 ../src/pragha-playlists-mgmt.c:1337
#: ../src/pragha-playlists-mgmt.c:1373
msgid "Export"
msgstr "Exportar"

#. Set dialog properties
#: ../src/pragha-library-pane.c:254 ../src/pragha-tags-dialog.c:147
msgid "Edit tags"
msgstr "Editar tags"

#: ../src/pragha-library-pane.c:256
msgid "Move to _trash"
msgstr "Mover para a _lixeira"

#: ../src/pragha-library-pane.c:258
msgid "Delete from library"
msgstr "Apagar da biblioteca"

#: ../src/pragha-library-pane.c:511 ../src/pragha-tags-dialog.c:105
msgid "Unknown"
msgstr "Desconhecido"

#: ../src/pragha-library-pane.c:523
msgid "Unknown Genre"
msgstr "Gênero Desconhecido"

#: ../src/pragha-library-pane.c:843
msgid "File can't be moved to trash. Delete permanently?"
msgstr ""

#: ../src/pragha-library-pane.c:844
#, c-format
msgid "The file \"%s\" cannot be moved to the trash. Details: %s"
msgstr "O arquivo \"%s\" não pode ser movido para a lixeira. Detalhes: %s"

#: ../src/pragha-library-pane.c:852 ../src/pragha-playlists-mgmt.c:85
#: ../src/pragha-playlists-mgmt.c:517 ../src/pragha-playlists-mgmt.c:589
#: ../src/pragha-playlists-mgmt.c:1239 ../src/pragha-preferences-dialog.c:671
#: ../src/pragha-preferences-dialog.c:1276 ../src/pragha-tags-dialog.c:149
#: ../src/pragha.c:209 ../src/pragha.c:408
#: ../plugins/lastfm/pragha-lastfm-plugin.c:956
#: ../plugins/tunein/pragha-tunein-plugin.c:227
msgid "_Cancel"
msgstr ""

#: ../src/pragha-library-pane.c:858
msgid "_Delete"
msgstr ""

#: ../src/pragha-library-pane.c:1895 ../src/pragha-library-pane.c:1977
#: ../src/pragha-playlists-mgmt.c:618 ../src/pragha-playlists-mgmt.c:645
#: ../src/pragha.c:306
msgid "Playlists"
msgstr "Listas de reprodução"

#: ../src/pragha-library-pane.c:1911 ../src/pragha-library-pane.c:1986
msgid "Radios"
msgstr "Rádios"

#: ../src/pragha-library-pane.c:1927 ../src/pragha-preferences-dialog.c:959
#: ../src/pragha-preferences-dialog.c:1283
msgid "Library"
msgstr "Biblioteca"

#: ../src/pragha-library-pane.c:2484
msgid "Really want to move the files to trash?"
msgstr "Você realmente deseja mover os arquivos para a lixeira?"

#: ../src/pragha-library-pane.c:2486
msgid "Delete permanently instead of moving to trash"
msgstr ""

#: ../src/pragha-library-pane.c:2536
msgid ""
"Are you sure you want to delete current file from library?\n"
"\n"
"Warning: To recover we must rescan the entire library."
msgstr ""
"Tem certeza de quer quer excluir o arquivo atual da biblioteca?\n"
"\n"
"Atenção: para recuperar é preciso re-escanear a biblioteca inteira."

#: ../src/pragha-menubar.c:190
msgid "_Playback"
msgstr "_Reprodução"

#: ../src/pragha-menubar.c:191
msgid "Play_list"
msgstr "_Lista de reprodução"

#: ../src/pragha-menubar.c:192
msgid "_View"
msgstr "_Visualização"

#: ../src/pragha-menubar.c:193 ../src/pragha-playlist.c:222
msgid "_Tools"
msgstr "_Ferramentas"

#: ../src/pragha-menubar.c:194
msgid "_Help"
msgstr "A_juda"

#: ../src/pragha-menubar.c:195 ../src/pragha-statusicon.c:83
#: ../plugins/notify/pragha-notify-plugin.c:177
msgid "Previous track"
msgstr ""

#: ../src/pragha-menubar.c:197 ../src/pragha-statusicon.c:85
msgid "Play / Pause"
msgstr "Reproduzir / Pausar"

#: ../src/pragha-menubar.c:199 ../src/pragha-statusicon.c:87
msgid "Stop"
msgstr "Parar"

#: ../src/pragha-menubar.c:201 ../src/pragha-statusicon.c:89
#: ../plugins/notify/pragha-notify-plugin.c:181
msgid "Next track"
msgstr "Próxima faixa"

#: ../src/pragha-menubar.c:203 ../src/pragha-playlist.c:225
#: ../src/pragha-statusicon.c:91
msgid "Edit track information"
msgstr "Editar informações da faixa"

#: ../src/pragha-menubar.c:205 ../src/pragha-statusicon.c:93
msgid "_Quit"
msgstr "_Sair"

#: ../src/pragha-menubar.c:207 ../src/pragha-statusicon.c:79
msgid "_Add files"
msgstr "_Adicionar arquivos"

#: ../src/pragha-menubar.c:208
msgid "Open a media file"
msgstr "Abrir um arquivo de mídia"

#: ../src/pragha-menubar.c:209 ../src/pragha-statusicon.c:81
msgid "Add _location"
msgstr "Adicionar _localização"

#: ../src/pragha-menubar.c:211
msgid "_Add the library"
msgstr "_Adicionar à biblioteca"

#: ../src/pragha-menubar.c:213
msgid "Remove selection from playlist"
msgstr "Remover seleção da lista de reprodução"

#: ../src/pragha-menubar.c:215 ../src/pragha-playlist.c:215
msgid "Crop playlist"
msgstr "Cortar lista de reprodução"

#: ../src/pragha-menubar.c:217 ../src/pragha-playlist.c:217
msgid "Clear playlist"
msgstr "Limpar lista de reprodução"

#: ../src/pragha-menubar.c:219 ../src/pragha-playlist.c:219
#: ../src/pragha-playlists-mgmt.c:69 ../src/pragha-playlists-mgmt.c:92
#: ../src/pragha-playlists-mgmt.c:1244
msgid "Save playlist"
msgstr "Salvar lista de reprodução"

#: ../src/pragha-menubar.c:220 ../src/pragha-menubar.c:225
#: ../src/pragha-playlists-mgmt.c:1333 ../src/pragha-playlists-mgmt.c:1369
msgid "New playlist"
msgstr "Nova lista de reprodução"

#: ../src/pragha-menubar.c:224 ../src/pragha-playlist.c:220
#: ../src/pragha-playlists-mgmt.c:71 ../src/pragha-playlists-mgmt.c:94
#: ../src/pragha-playlists-mgmt.c:1246
msgid "Save selection"
msgstr "Salvar seleção"

#: ../src/pragha-menubar.c:229
msgid "_Search in playlist"
msgstr "_Procurar na lista de reprodução"

#: ../src/pragha-menubar.c:231
msgid "_Preferences"
msgstr "_Preferências"

#: ../src/pragha-menubar.c:233
msgid "Jump to playing song"
msgstr "Ir para a música atual"

#: ../src/pragha-menubar.c:235
msgid "E_qualizer"
msgstr "E_qualizador"

#: ../src/pragha-menubar.c:237
msgid "_Rescan library"
msgstr "_Varrer biblioteca"

#: ../src/pragha-menubar.c:239
#: ../plugins/removable-media/pragha-devices-removable.c:302
msgid "_Update library"
msgstr "_Atualizar biblioteca"

#: ../src/pragha-menubar.c:241
msgid "_Statistics"
msgstr "_Estatísticas"

#: ../src/pragha-menubar.c:243
msgid "Homepage"
msgstr "Página Inicial"

#: ../src/pragha-menubar.c:245
msgid "Community"
msgstr "Comunidade"

#: ../src/pragha-menubar.c:247
msgid "Wiki"
msgstr "Wiki"

#: ../src/pragha-menubar.c:249
msgid "Translate Pragha"
msgstr "Traduzir o Pragha"

#: ../src/pragha-menubar.c:251 ../src/pragha-statusicon.c:77
msgid "About"
msgstr "Sobre"

#: ../src/pragha-menubar.c:256
msgid "_Shuffle"
msgstr "_Aleatório"

#: ../src/pragha-menubar.c:259
msgid "_Repeat"
msgstr "_Repetir"

#: ../src/pragha-menubar.c:262
msgid "_Fullscreen"
msgstr "_Tela cheia"

#: ../src/pragha-menubar.c:265
msgid "Lateral _panel"
msgstr "_Painel lateral"

#: ../src/pragha-menubar.c:268
msgid "Secondary lateral panel"
msgstr ""

#: ../src/pragha-menubar.c:271
msgid "Playback controls below"
msgstr "Controles de reprodução abaixo"

#: ../src/pragha-menubar.c:274
msgid "Menubar"
msgstr ""

#: ../src/pragha-menubar.c:277
msgid "Status bar"
msgstr "Barra de status"

#: ../src/pragha-menubar.c:638
msgid "Total Tracks:"
msgstr "Total de faixas:"

#: ../src/pragha-menubar.c:640
msgid "Total Artists:"
msgstr "Total de Artistas:"

#: ../src/pragha-menubar.c:642
msgid "Total Albums:"
msgstr "Total de Álbuns:"

#: ../src/pragha-menubar.c:645
msgid "Statistics"
msgstr "Estatísticas"

#. Create labels
#: ../src/pragha-playlist.c:114 ../src/pragha-playlist.c:3370
#: ../src/pragha-playlist.c:3546 ../src/pragha-statusicon.c:179
#: ../src/pragha-tags-dialog.c:167 ../src/pragha-tags-dialog.c:1077
msgid "Title"
msgstr "Título"

#: ../src/pragha-playlist.c:118 ../src/pragha-playlist.c:3374
#: ../src/pragha-playlist.c:3550 ../src/pragha-tags-dialog.c:682
msgid "Bitrate"
msgstr "Taxa de bits"

#: ../src/pragha-playlist.c:119 ../src/pragha-playlist.c:3375
#: ../src/pragha-playlist.c:3551 ../src/pragha-tags-dialog.c:172
msgid "Year"
msgstr "Ano"

#: ../src/pragha-playlist.c:120 ../src/pragha-playlist.c:3376
#: ../src/pragha-playlist.c:3552 ../src/pragha-tags-dialog.c:173
#: ../src/pragha-tags-dialog.c:1097
msgid "Comment"
msgstr "Comentário"

#. Create labels
#: ../src/pragha-playlist.c:121 ../src/pragha-playlist.c:3377
#: ../src/pragha-playlist.c:3553 ../src/pragha-statusicon.c:182
#: ../src/pragha-tags-dialog.c:681
msgid "Length"
msgstr "Tempo"

#: ../src/pragha-playlist.c:122 ../src/pragha-playlist.c:3378
#: ../src/pragha-playlist.c:3554 ../src/pragha-tags-dialog.c:686
msgid "Filename"
msgstr "Nome do Arquivo"

#: ../src/pragha-playlist.c:123 ../src/pragha-playlist.c:3379
#: ../src/pragha-playlist.c:3555 ../src/pragha-tags-dialog.c:687
msgid "Mimetype"
msgstr ""

#: ../src/pragha-playlist.c:211
msgid "Remove from playback queue"
msgstr ""

#: ../src/pragha-playlist.c:213
msgid "Remove from playlist"
msgstr "Remover da lista de reprodução"

#: ../src/pragha-playlist.c:221
msgid "_Send to"
msgstr ""

#: ../src/pragha-playlist.c:2501
#, c-format
msgid "Copy \"%i\" to selected track numbers"
msgstr "Copiar \"%i\" para os números de faixas selecionadas"

#: ../src/pragha-playlist.c:2507
#, c-format
msgid "Copy \"%s\" to selected titles"
msgstr "Copiar \"%s\" para arquivos selecionados"

#: ../src/pragha-playlist.c:2513
#, c-format
msgid "Copy \"%s\" to selected artists"
msgstr "Copiar \"%s\" para artistas selecionados"

#: ../src/pragha-playlist.c:2519
#, c-format
msgid "Copy \"%s\" to selected albums"
msgstr "Copiar \"%s\" para álbuns selecionados"

#: ../src/pragha-playlist.c:2525
#, c-format
msgid "Copy \"%s\" to selected genres"
msgstr "Copiar \"%s\" para gêneros selecionados"

#: ../src/pragha-playlist.c:2531
#, c-format
msgid "Copy \"%i\" to selected years"
msgstr "Copiar \"%i\" para anos selecionados"

#: ../src/pragha-playlist.c:2537
#, c-format
msgid "Copy \"%s\" to selected comments"
msgstr "Copiar \"%s\" para comentários selecionados"

#. Create the checkmenu items
#: ../src/pragha-playlist.c:3369 ../src/pragha-playlist.c:3545
#: ../src/pragha.c:626
msgid "Track"
msgid_plural "Tracks"
msgstr[0] "Faixa"
msgstr[1] "Faixas"

#: ../src/pragha-playlist.c:3382
msgid "Clear sort"
msgstr "Limpar ordem"

#: ../src/pragha-playlists-mgmt.c:73
msgid "Playlist"
msgstr ""

#: ../src/pragha-playlists-mgmt.c:86 ../src/pragha-playlists-mgmt.c:518
#: ../src/pragha-playlists-mgmt.c:1240 ../src/pragha-preferences-dialog.c:1277
#: ../src/pragha-tags-dialog.c:150 ../src/pragha-tags-dialog.c:767
#: ../src/pragha.c:409 ../plugins/song-info/pragha-song-info-dialog.c:84
#: ../plugins/tunein/pragha-tunein-plugin.c:228
msgid "_Ok"
msgstr ""

#: ../src/pragha-playlists-mgmt.c:129
msgid "<b>con_playlist</b> is a reserved playlist name"
msgstr "<b>con_playlist</b> é um nome reservado"

#: ../src/pragha-playlists-mgmt.c:153
#, c-format
msgid "Do you want to overwrite the playlist: %s ?"
msgstr "Você deseja sobrescrever a lista de reprodução: %s?"

#: ../src/pragha-playlists-mgmt.c:507
msgid "Choose a new name"
msgstr "Escolha um novo nome"

#: ../src/pragha-playlists-mgmt.c:556
#, c-format
msgid "Do you want to delete the item: %s ?"
msgstr "Você deseja excluir o item: %s?"

#: ../src/pragha-playlists-mgmt.c:586
msgid "Export playlist to file"
msgstr "Exportar lista de reprodução para arquivo"

#: ../src/pragha-playlists-mgmt.c:590
msgid "_Save"
msgstr ""

#: ../src/pragha-playlists-mgmt.c:1036
#: ../plugins/lastfm/pragha-lastfm-plugin.c:929
#, c-format
msgid "Added %d songs from %d of the imported playlist."
msgstr "Adicionadas %d de %d faixas da lista de reprodução importada"

#: ../src/pragha-playlists-mgmt.c:1224
msgid "What do you want to do?"
msgstr ""

#: ../src/pragha-playlists-mgmt.c:1226
#, c-format
msgid "Replace the playlist \"%s\""
msgstr ""

#: ../src/pragha-playlists-mgmt.c:1231
#, c-format
msgid "Add to playlist \"%s\""
msgstr ""

#: ../src/pragha-preferences-dialog.c:192
msgid ""
"Patterns should be of the form:<filename>;<filename>;....\n"
"A maximum of six patterns are allowed.\n"
"Wildcards are not accepted as of now ( patches welcome :-) )."
msgstr ""
"Os padrões devem ser na forma:<nomedoarquivo>;<nomedoarquivo>;....\n"
"É permitido um máximo de 6 padrões.\n"
"Por enquanto curingas não são aceitos (mas correções são bem vindas :-) )."

#: ../src/pragha-preferences-dialog.c:215
msgid "Album art pattern"
msgstr "Nome da arte do álbum"

#: ../src/pragha-preferences-dialog.c:523
#: ../src/pragha-preferences-dialog.c:1132
msgid "Start normal"
msgstr "Iniciar normal"

#: ../src/pragha-preferences-dialog.c:527
#: ../src/pragha-preferences-dialog.c:1133
msgid "Start fullscreen"
msgstr "Abrir tela cheia"

#: ../src/pragha-preferences-dialog.c:531
#: ../src/pragha-preferences-dialog.c:1134
msgid "Start in system tray"
msgstr "Iniciar na bandeja"

#. Create a folder chooser dialog
#: ../src/pragha-preferences-dialog.c:668
msgid "Select a folder to add to library"
msgstr "Selecione uma pasta para adicionar à biblioteca"

#: ../src/pragha-preferences-dialog.c:672
#: ../plugins/lastfm/pragha-lastfm-plugin.c:957
msgid "_Open"
msgstr ""

#: ../src/pragha-preferences-dialog.c:895
#: ../src/pragha-preferences-dialog.c:1302
msgid "Audio"
msgstr "Áudio"

#: ../src/pragha-preferences-dialog.c:897
msgid "Audio sink"
msgstr "Sistema de Som"

#: ../src/pragha-preferences-dialog.c:901
#: ../src/pragha-preferences-dialog.c:920
#: ../src/pragha-preferences-dialog.c:926
msgid "Restart Required"
msgstr "Necessário Reiniciar"

#: ../src/pragha-preferences-dialog.c:916
msgid "Audio Device"
msgstr "Dispositivo de Som"

#: ../src/pragha-preferences-dialog.c:925
msgid "Use software mixer"
msgstr "Usar controle do programa"

#: ../src/pragha-preferences-dialog.c:963
msgid "Can not change directories while they are analyzing."
msgstr ""

#: ../src/pragha-preferences-dialog.c:973
msgid "Folders"
msgstr "Pasta"

#: ../src/pragha-preferences-dialog.c:992 ../src/pragha.c:210
msgid "_Add"
msgstr ""

#: ../src/pragha-preferences-dialog.c:993
msgid "_Remove"
msgstr ""

#: ../src/pragha-preferences-dialog.c:1009
msgid "Merge folders in the folders estructure view"
msgstr "Mesclar pastas na visão de estrutura de pastas"

#: ../src/pragha-preferences-dialog.c:1012
msgid "Sort albums by release year"
msgstr "Mostrar álbuns por ano de lançamento"

#. Labels
#: ../src/pragha-preferences-dialog.c:1055
#: ../src/pragha-preferences-dialog.c:1282
msgid "Appearance"
msgstr "Aparência"

#: ../src/pragha-preferences-dialog.c:1057
msgid "Use system title bar and borders"
msgstr ""

#: ../src/pragha-preferences-dialog.c:1064
msgid "Use small icons on the toolbars"
msgstr ""

#: ../src/pragha-preferences-dialog.c:1067
msgid "Controls"
msgstr ""

#: ../src/pragha-preferences-dialog.c:1069
msgid "Show Album art in Panel"
msgstr "Mostrar arte do álbum no painel"

#: ../src/pragha-preferences-dialog.c:1072
msgid "Size of Album art"
msgstr "Tamanho da arte do álbum"

#: ../src/pragha-preferences-dialog.c:1077
msgid "Album art file pattern"
msgstr "Nome da arte do álbum"

#: ../src/pragha-preferences-dialog.c:1120
msgid "Search"
msgstr ""

#. Instant search.
#: ../src/pragha-preferences-dialog.c:1122 ../src/pragha-search-entry.c:41
msgid "Search while typing"
msgstr ""

#. Aproximate search.
#: ../src/pragha-preferences-dialog.c:1125 ../src/pragha-search-entry.c:48
msgid "Search similar words"
msgstr ""

#: ../src/pragha-preferences-dialog.c:1128
msgid "When starting pragha"
msgstr ""

#: ../src/pragha-preferences-dialog.c:1131
msgid "Remember last window state"
msgstr "Lembrar do estado da última janela"

#: ../src/pragha-preferences-dialog.c:1137
msgid "Restore last playlist"
msgstr "Restaurar a última lista de reprodução"

#: ../src/pragha-preferences-dialog.c:1140
msgid "When adding folders"
msgstr ""

#: ../src/pragha-preferences-dialog.c:1141 ../src/pragha.c:201
msgid "Add files recursively"
msgstr "Adicionar arquivos recursivamente"

#: ../src/pragha-preferences-dialog.c:1165
#: ../src/pragha-preferences-dialog.c:1317
msgid "Desktop"
msgstr ""

#: ../src/pragha-preferences-dialog.c:1167
msgid "Show Pragha icon in the notification area"
msgstr ""

#: ../src/pragha-preferences-dialog.c:1170
msgid "Minimize Pragha when closing window"
msgstr ""

#: ../src/pragha-preferences-dialog.c:1191
#: ../src/pragha-preferences-dialog.c:1286
msgid "Plugins"
msgstr ""

#. The main preferences dialog
#: ../src/pragha-preferences-dialog.c:1273
msgid "Preferences"
msgstr ""

#: ../src/pragha-preferences-dialog.c:1284
msgid "General"
msgstr "Geral"

#: ../src/pragha-preferences-dialog.c:1321
msgid "Services"
msgstr ""

#: ../src/pragha-preferences-dialog.c:1335
msgid "Preferences of Pragha"
msgstr "Preferências do Pragha"

#: ../src/pragha-scanner.c:93
#, c-format
msgid "%i files analized of %i detected"
msgstr ""

#: ../src/pragha-scanner.c:99
msgid "Searching files to analyze"
msgstr ""

#: ../src/pragha-scanner.c:250
msgid "Library scan complete"
msgstr "Varredura da biblioteca completa"

#: ../src/pragha-statusicon.c:174 ../src/pragha-toolbar.c:174
#: ../src/pragha-toolbar.c:524
msgid "<b>Not playing</b>"
msgstr "<b>Parado</b>"

#: ../src/pragha-tags-dialog.c:171
msgid "Track No"
msgstr "Faixa nº"

#: ../src/pragha-tags-dialog.c:174
msgid "File"
msgstr "Arquivo"

#. The main edit dialog
#: ../src/pragha-tags-dialog.c:562 ../src/pragha-tags-dialog.c:764
msgid "Details"
msgstr "Detalhes"

#: ../src/pragha-tags-dialog.c:664
msgid "channel"
msgid_plural "channels"
msgstr[0] ""
msgstr[1] ""

#: ../src/pragha-tags-dialog.c:683
msgid "Channels"
msgstr "Canais"

#: ../src/pragha-tags-dialog.c:684
msgid "Samplerate"
msgstr "Taxa de amostragem"

#: ../src/pragha-tags-dialog.c:685
msgid "Folder"
msgstr "Pasta"

#: ../src/pragha-tags-dialog.c:1070
msgid "Selection to"
msgstr "Selecionar para"

#: ../src/pragha-tags-dialog.c:1108
msgid "Open folder"
msgstr "Abrir pasta"

#: ../src/pragha-tags-mgmt.c:197
#, c-format
msgid ""
"Do you want to set the track number of ALL of the selected tracks to: %d ?"
msgstr ""
"Você deseja definir o número de faixa de TODAS as faixas selecionadas para: "
"%d ?"

#: ../src/pragha-tags-mgmt.c:215
#, c-format
msgid "Do you want to set the title tag of ALL of the selected tracks to: %s ?"
msgstr ""
"Você deseja definir o tag do título de TODAS as faixas selecionadas para: "
"%s ?"

#: ../src/pragha-toolbar.c:150
#, c-format
msgid ""
"%s <small><span weight=\"light\">by</span></small> %s <small><span weight="
"\"light\">in</span></small> %s"
msgstr ""
"%s <small><span weight=\"light\">por</span></small> %s <small><span weight="
"\"light\">em</span></small> %s"

#: ../src/pragha-toolbar.c:155
#: ../plugins/song-info/pragha-song-info-thread-dialog.c:52
#, c-format
msgid "%s <small><span weight=\"light\">by</span></small> %s"
msgstr "%s <small><span weight=\"light\">por</span></small> %s"

#: ../src/pragha-toolbar.c:159
#, c-format
msgid "%s <small><span weight=\"light\">in</span></small> %s"
msgstr "%s <small><span weight=\"light\">em</span></small> %s"

#: ../src/pragha-toolbar.c:809
msgid "Previous Track"
msgstr "Faixa anterior"

#: ../src/pragha-toolbar.c:813
msgid "Play / Pause Track"
msgstr "Reproduzir / Pausar faixa"

#: ../src/pragha-toolbar.c:817
msgid "Stop playback"
msgstr "Parar reprodução"

#: ../src/pragha-toolbar.c:821
msgid "Next Track"
msgstr "Próxima faixa"

#: ../src/pragha-toolbar.c:853
msgid "Leave Fullscreen"
msgstr "Sair da tela cheia"

#: ../src/pragha-toolbar.c:857
msgid "Play songs in a random order"
msgstr "Reproduzir em ordem aleatória"

#: ../src/pragha-toolbar.c:860
msgid "Repeat playback list at the end"
msgstr "Repetir lista de reprodução"

#: ../src/pragha-utils.c:239
msgid "day"
msgid_plural "days"
msgstr[0] ""
msgstr[1] "dia"

#: ../src/pragha-utils.c:486
msgid "Unable to open the browser"
msgstr "Não foi possível abrir o navegador"

#: ../src/pragha-window.c:115
#, c-format
msgid ""
"<b>Error playing current track.</b>\n"
"(%s)\n"
"<b>Reason:</b> %s"
msgstr ""
"<b>Erro ao reproduzir a faixa atual.</b>\n"
"(%s)\n"
"<b>Motivo:</b> %s"

#: ../src/pragha-window.c:118
msgid "_Stop"
msgstr ""

#: ../src/pragha-window.c:119
msgid "_Next"
msgstr ""

#: ../src/pragha.c:176
msgid "Select a file to play"
msgstr "Selecione um arquivo para reproduzir"

#: ../src/pragha.c:223 ../plugins/lastfm/pragha-lastfm-plugin.c:961
msgid "Supported media"
msgstr "Mídias suportados"

#: ../src/pragha.c:309
msgid "All files"
msgstr "Todos os arquivos"

#: ../src/pragha.c:385
msgid "Enter the URL of an internet radio stream"
msgstr "Digite a URL de um fluxo de rádio de Internet"

#: ../src/pragha.c:392
msgid "Give it a name to save"
msgstr "Dê um nome para salvar"

#: ../src/pragha.c:405
msgid "Add a location"
msgstr "Adicionar uma localização"

#: ../src/pragha.c:537
msgid "translator-credits"
msgstr "translator-credits"

#. Setup application name and pulseaudio role
#: ../src/pragha.c:705 ../src/pragha.c:937 ../src/pragha.c:1314
#: ../plugins/dlna/pragha-dlna-plugin.c:222
#: ../plugins/dlna/pragha-dlna-plugin.c:224
msgid "Pragha Music Player"
msgstr ""

#: ../data/pragha.desktop.in.h:1
msgid "Music Player"
msgstr ""

#: ../data/pragha.desktop.in.h:2
msgid "Manage and listen to music"
msgstr ""

#: ../plugins/acoustid/pragha-acoustid-plugin.c:99
#: ../plugins/acoustid/pragha-acoustid-plugin.c:428
msgid "Search tags on AcoustID"
msgstr ""

#: ../plugins/acoustid/pragha-acoustid-plugin.c:261
msgid "AcoustID not found any similar song"
msgstr ""

#: ../plugins/cdrom/pragha-cdrom-plugin.c:477
msgid "Audio/Data CD"
msgstr ""

#: ../plugins/cdrom/pragha-cdrom-plugin.c:478
msgid "An audio CD was inserted"
msgstr ""

#: ../plugins/cdrom/pragha-cdrom-plugin.c:479
#: ../plugins/cdrom/pragha-cdrom-plugin.c:519
#: ../plugins/cdrom/pragha-cdrom-plugin.c:730
msgid "Add Audio _CD"
msgstr "Adicionar _CD de áudio"

#: ../plugins/cdrom/pragha-cdrom-plugin.c:622
msgid "Audio CD"
msgstr ""

#: ../plugins/cdrom/pragha-cdrom-plugin.c:624
msgid "Audio CD Device"
msgstr "Dispositivo do CD de Áudio"

#: ../plugins/cdrom/pragha-cdrom-plugin.c:644
msgid "Connect to CDDB server"
msgstr "Conectar ao servidor CDDB"

#: ../plugins/devices/pragha-device-client.c:76
msgid "Ignore"
msgstr ""

#: ../plugins/dlna-renderer/pragha-dlna-renderer-plugin.c:89
#: ../plugins/dlna-renderer/pragha-dlna-renderer-plugin.c:283
msgid "Search music on DLNA server"
msgstr ""

#: ../plugins/dlna-renderer/pragha-dlna-renderer-plugin.c:220
#, c-format
msgid "Music of the %s server was added."
msgstr ""

#: ../plugins/dlna-renderer/pragha-dlna-renderer-plugin.c:225
msgid "Could not find any DLNA server."
msgstr ""

#: ../plugins/lastfm/pragha-lastfm-plugin.c:174
#: ../plugins/lastfm/pragha-lastfm-plugin.c:1549
msgid "_Lastfm"
msgstr "_Last.fm"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:175
#: ../plugins/lastfm/pragha-lastfm-plugin.c:215
msgid "Love track"
msgstr "Música favorita"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:177
#: ../plugins/lastfm/pragha-lastfm-plugin.c:217
msgid "Unlove track"
msgstr "Não é favorita"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:179
#: ../plugins/lastfm/pragha-lastfm-plugin.c:953
msgid "Import a XSPF playlist"
msgstr "Importar uma lista XSPF"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:181
msgid "Add favorites"
msgstr "Adicionar favoritos"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:183
#: ../plugins/lastfm/pragha-lastfm-plugin.c:219
msgid "Add similar"
msgstr "Adicionar semelhante"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:432
msgid "Unable to establish conection with Last.fm"
msgstr ""

#: ../plugins/lastfm/pragha-lastfm-plugin.c:617
msgid "Last.fm suggested a tag correction"
msgstr "Last.fm sugeriu uma correção de tag"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:643
msgid "Love song on Last.fm failed."
msgstr "Falha ao enviar dados para Last.fm"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:661
msgid "Unlove song on Last.fm failed."
msgstr ""

#: ../plugins/lastfm/pragha-lastfm-plugin.c:768
#, c-format
msgid "Added %d tracks of %d suggested from Last.fm"
msgstr ""

#: ../plugins/lastfm/pragha-lastfm-plugin.c:771
#, c-format
msgid "Last.fm doesn't suggest any similar track"
msgstr ""

#: ../plugins/lastfm/pragha-lastfm-plugin.c:775
#, c-format
msgid "Added %d songs of the last %d loved on Last.fm."
msgstr "Adicionadas %d últimas músicas %d favoritas no Last.fm."

#: ../plugins/lastfm/pragha-lastfm-plugin.c:778
#, c-format
msgid "You don't have favorite tracks on Last.fm"
msgstr ""

#: ../plugins/lastfm/pragha-lastfm-plugin.c:1248
msgid "Last.fm submission failed"
msgstr ""

#: ../plugins/lastfm/pragha-lastfm-plugin.c:1250
msgid "Track scrobbled on Last.fm"
msgstr "Feito scrobble da faixa no Last.fm"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:1360
msgid "Update current song on Last.fm failed."
msgstr "Falha ao atualizar faixa no Last.fm"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:1787
msgid "Scrobble on Last.fm"
msgstr ""

#: ../plugins/lastfm/pragha-lastfm-plugin.c:1790
msgid "Username"
msgstr "Usuário"

#: ../plugins/lastfm/pragha-lastfm-plugin.c:1797
msgid "Password"
msgstr "Senha"

#: ../plugins/mpris2/pragha-mpris2-plugin.c:856
#, fuzzy
msgid "Tracks"
msgstr "Faixa"

#: ../plugins/notify/pragha-notify-plugin.c:165
#, c-format
msgid "by <b>%s</b> in <b>%s</b> <b>(%s)</b>"
msgstr "por <b>%s</b> em <b>%s</b> <b>(%s)</b>"

#: ../plugins/notify/pragha-notify-plugin.c:267
msgid "Notifications"
msgstr "Notificações"

#: ../plugins/notify/pragha-notify-plugin.c:269
msgid "Show Album art in notifications"
msgstr "Mostrar arte do álbum nas notificações"

#: ../plugins/notify/pragha-notify-plugin.c:274
msgid "Add actions to change track in notifications"
msgstr ""

#: ../plugins/removable-media/pragha-devices-removable.c:211
#, c-format
msgid "Unable to access \"%s\""
msgstr ""

#: ../plugins/removable-media/pragha-devices-removable.c:214
#: ../plugins/removable-media/pragha-devices-removable.c:300
msgid "Removable Device"
msgstr ""

#: ../plugins/removable-media/pragha-devices-removable.c:297
#, c-format
msgid "Want to manage \"%s\" volume?"
msgstr ""

#: ../plugins/song-info/pragha-song-info-plugin.c:88
msgid "Search _lyric"
msgstr ""

#: ../plugins/song-info/pragha-song-info-plugin.c:90
msgid "Search _artist info"
msgstr ""

#: ../plugins/song-info/pragha-song-info-plugin.c:377
msgid "Song Information"
msgstr ""

#: ../plugins/song-info/pragha-song-info-plugin.c:379
msgid "Download the album art while playing their songs."
msgstr ""

#: ../plugins/song-info/pragha-song-info-thread-dialog.c:51
#, c-format
msgid "Lyrics thanks to %s"
msgstr ""

#: ../plugins/song-info/pragha-song-info-thread-dialog.c:58
#, c-format
msgid "Artist info"
msgstr ""

#: ../plugins/song-info/pragha-song-info-thread-dialog.c:59
#, c-format
msgid "%s <small><span weight=\"light\">thanks to</span></small> %s"
msgstr ""

#: ../plugins/song-info/pragha-song-info-thread-dialog.c:81
#: ../plugins/song-info/pragha-song-info-thread-pane.c:100
msgid "Lyrics not found."
msgstr ""

#: ../plugins/song-info/pragha-song-info-thread-dialog.c:84
#: ../plugins/song-info/pragha-song-info-thread-pane.c:104
msgid "Artist information not found."
msgstr ""

#: ../plugins/song-info/pragha-song-info-thread-pane.c:185
#: ../plugins/song-info/pragha-song-info-thread-pane.c:193
msgid "Searching..."
msgstr ""

#: ../plugins/tunein/pragha-tunein-plugin.c:92
#: ../plugins/tunein/pragha-tunein-plugin.c:295
msgid "Search radio on TuneIn"
msgstr ""

#: ../plugins/tunein/pragha-tunein-plugin.c:224
#: ../plugins/tunein/pragha-tunein-plugin.c:235
msgid "Search in TuneIn"
msgstr ""

#~ msgid "Highlight rows on current playlist"
#~ msgstr "Destacar linhas na lista de reprodução atual"
